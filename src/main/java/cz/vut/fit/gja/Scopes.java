package cz.vut.fit.gja;

/**
 *
 */
public class Scopes {
    public static final String PROTOTYPE = "prototype";
    public static final String SESSION = "session";
    public static final String VIEW = "view";
    public static final String REQUEST = "request";
    public static final String WINDOW = "window";
}
