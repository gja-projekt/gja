
package cz.vut.fit.gja.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author 
 */
@Entity
@Table(name = "tag")
@NamedQueries({
    @NamedQuery(name = Tag.FIND_ALL, query = "SELECT t FROM Tag t"),
    @NamedQuery(name = Tag.FIND_ALL_ORDER_BY_RELEVANCE, query = "SELECT t FROM Tag t ORDER BY relevance desc"),
    @NamedQuery(name = Tag.FIND_BY_ID, query = "SELECT t FROM Tag t WHERE t.id = :id"),
    @NamedQuery(name = Tag.FIND_BY_NAME, query = "SELECT t FROM Tag t WHERE t.name = :name"),
    @NamedQuery(name = Tag.FIND_BY_RELEVANCE, query = "SELECT t FROM Tag t WHERE t.relevance = :relevance"),
    @NamedQuery(name = Tag.AGREGATE_MAX_RELEVANCE, query = "SELECT MAX(t.relevance) FROM Tag t")})
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String FIND_ALL = "Tag.findAll";
    public static final String FIND_ALL_ORDER_BY_RELEVANCE = "Tag.findAllOrderByRelevance";
    public static final String FIND_BY_ID = "Tag.findById";
    public static final String FIND_BY_NAME = "Tag.findByName";
    public static final String FIND_BY_RELEVANCE = "Tag.findByRelevance";
    public static final String AGREGATE_MAX_RELEVANCE = "Tag.agregateMaxRelevance";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;

    @Lob
    @NotNull
    @Size(max = 100)
    @Column(length = 100, nullable = false, columnDefinition = "VARCHAR")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int relevance;

    @ManyToMany(mappedBy = "tagList")
    private List<Project> projectList;

    public Tag() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStringId() {
        return id.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRelevance() {
        return relevance;
    }

    public void setRelevance(int relevance) {
        this.relevance = relevance;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tag)) {
            return false;
        }
        Tag other = (Tag) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.vut.fit.gja.entities.Tag[ id=" + id + ", name=" + name + " ]";
    }
    
}
