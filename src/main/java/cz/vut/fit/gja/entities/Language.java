
package cz.vut.fit.gja.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author
 */
@Entity
@Table(name = "language")
@NamedQueries({
    @NamedQuery(name = Language.FIND_ALL, query = "SELECT l FROM Language l"),
    @NamedQuery(name = Language.FIND_BY_ID, query = "SELECT l FROM Language l WHERE l.id = :id"),
    @NamedQuery(name = Language.FIND_BY_EXTENSION, query = "SELECT l FROM Language l WHERE l.extension = :extension"),
    @NamedQuery(name = Language.FIND_BY_HIGHLIGHTMARK, query = "SELECT l FROM Language l WHERE l.highlightMark = :highlightMark")})
public class Language implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String FIND_ALL = "Language.findAll";
    public static final String FIND_BY_ID = "Language.findById";
    public static final String FIND_BY_EXTENSION = "Language.findByExtension";
    public static final String FIND_BY_HIGHLIGHTMARK = "Language.findByHighlightMark";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(nullable = false, length = 40)
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private String extension;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "highlight_mark", nullable = false, length = 20)
    private String highlightMark;

    @OneToMany(mappedBy = "language")
    private List<Project> projectList;

    public Language() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getHighlightMark() {
        return highlightMark;
    }

    public void setHighlightMark(String highlightMark) {
        this.highlightMark = highlightMark;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Language)) {
            return false;
        }
        Language other = (Language) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.vut.fit.gja.entities.Language[ id=" + id + " ]";
    }
    
}
