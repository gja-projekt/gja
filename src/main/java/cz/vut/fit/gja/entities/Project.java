
package cz.vut.fit.gja.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author
 */
@Entity
@Table(name = "project")
@NamedQueries({
    @NamedQuery(name = Project.FIND_ALL, query = "SELECT p FROM Project p"),
    @NamedQuery(name = Project.FIND_BY_ID, query = "SELECT p FROM Project p WHERE p.id = :id"),
    @NamedQuery(name = Project.FIND_BY_NAME, query = "SELECT p FROM Project p WHERE p.name = :name"),
    @NamedQuery(name = Project.FIND_BY_CREATED, query = "SELECT p FROM Project p WHERE p.created = :created"),
    @NamedQuery(name = Project.FIND_BY_EXPIRE, query = "SELECT p FROM Project p WHERE p.expire = :expire"),
    @NamedQuery(name = Project.FIND_BY_RATING, query = "SELECT p FROM Project p WHERE p.rating = :rating"),
    @NamedQuery(name = Project.FIND_BY_VOTES, query = "SELECT p FROM Project p WHERE p.votes = :votes"),
    @NamedQuery(name = Project.FIND_BY_DOWNLOAD_COUNT, query = "SELECT p FROM Project p WHERE p.downloadCount = :downloadCount"),
    @NamedQuery(name = Project.FIND_BY_LANGUAGE_NAME, query = "SELECT p FROM Project p WHERE p.language.name = :languagename"),
    @NamedQuery(name = Project.FETCH_MOST_DOWNLOADED, query = "SELECT p FROM Project p WHERE p.downloadCount > 0 ORDER BY p.downloadCount DESC"),
    @NamedQuery(name = Project.FETCH_MOST_RATED, query = "SELECT p FROM Project p ORDER BY p.rating DESC"),
    @NamedQuery(name = Project.AGREGATE_LANGUAGE_COUNT, query = "SELECT p.language, COUNT(p) AS c FROM Project p GROUP BY p.language.id ORDER BY c DESC"),
    @NamedQuery(name = Project.FIND_BY_TAG, query = "SELECT p FROM Project p JOIN p.tagList t WHERE t.name = :tagname"),
    @NamedQuery(name = Project.SEARCH, query = "SELECT p FROM Project p WHERE p.name like :query1 OR p.description like :query2")})
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String FIND_ALL = "Project.findAll";
    public static final String FIND_BY_ID = "Project.findById";
    public static final String FIND_BY_NAME = "Project.findByName";
    public static final String FIND_BY_CREATED = "Project.findByCreated";
    public static final String FIND_BY_EXPIRE = "Project.findByExpire";
    public static final String FIND_BY_RATING = "Project.findByRating";
    public static final String FIND_BY_VOTES = "Project.findByVotes";
    public static final String FIND_BY_DOWNLOAD_COUNT = "Project.findByDownloadCount";
    public static final String FIND_BY_LANGUAGE_NAME = "Project.findByLanguageName";
    public static final String FETCH_MOST_DOWNLOADED = "Project.fetchMostDownloaded";
    public static final String FETCH_MOST_RATED = "Project.fetchMostRated";
    public static final String AGREGATE_LANGUAGE_COUNT = "Project.agregateLanguageCount";
    public static final String FIND_BY_TAG = "Project.findByTag";
    public static final String SEARCH = "Project.search";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;

    @Size(max = 100)
    @Column
    private String name;

    @Lob
    @Size(max = 65535)
    @Column(length = 65535, columnDefinition = "TEXT")
    private String description;

    @Basic(optional = true)
    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expire;
    private short rating;
    // TODO: votes

    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int votes;

    @Basic(optional = false)
    @NotNull
    @Column(name = "download_count", nullable = false)
    private int downloadCount;

    @JoinTable(name = "project_tag", joinColumns = {
        @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "tag_id", referencedColumnName = "id", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Tag> tagList;

    @JoinColumn(name = "language_id", referencedColumnName = "id")
    @ManyToOne
    private Language language;

    public Project() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public short getRating() {
        return rating;
    }

    public void setRating(Short rating) {
        this.rating = rating;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.vut.fit.gja.entities.Project[ id=" + id + " ]";
    }

}
