package cz.vut.fit.gja.dao;

import cz.vut.fit.gja.entities.Project;

import java.util.List;

/**
 *
 */
public interface ProjectDao extends AbstractDao<Project> {

    List<Project> findProjectByName(String name);

    List<Project> mostDownloaded();

    List<Project> mostRated();

    @SuppressWarnings("unchecked")
    List<Object[]> aggregateLanguageCount();

    List<Project> findByLanguage(String languageName);

    List<Project> findByTag(String tagName);

    List<Project> findByQuery(String query);
}
