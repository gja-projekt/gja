
package cz.vut.fit.gja.dao.impl;

import cz.vut.fit.gja.dao.TagDao;
import cz.vut.fit.gja.entities.Tag;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;


/**
 *
 *
 */
@Transactional
@Repository
public class TagDaoImpl extends AbstractDaoImpl<Tag> implements TagDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TagDaoImpl() {
        super(Tag.class);
    }

    @Override
    public Tag findByName(String name) {
        try {
            return em.createNamedQuery(Tag.FIND_BY_NAME, Tag.class)
                .setParameter("name", name)
                .getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Integer aggregateMaxRelevance() {
        try {
            return em.createNamedQuery(Tag.AGREGATE_MAX_RELEVANCE, Integer.class).getSingleResult();
        } catch (NoResultException e) {
            return 0;
        }
    }

    @Override
    public List<Tag> findAllOrderByRelevance(int count) {
        return em.createNamedQuery(Tag.FIND_ALL_ORDER_BY_RELEVANCE, Tag.class)
                .setMaxResults(count)
                .getResultList();
    }
}
