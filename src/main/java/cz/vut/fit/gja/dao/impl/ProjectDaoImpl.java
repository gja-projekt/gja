package cz.vut.fit.gja.dao.impl;

import cz.vut.fit.gja.dao.ProjectDao;
import cz.vut.fit.gja.entities.Project;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 *
 */
@Transactional
@Repository
public class ProjectDaoImpl extends AbstractDaoImpl<Project> implements ProjectDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProjectDaoImpl() {
        super(Project.class);
    }

    @Override
    public List<Project> findProjectByName(String name) {
        return em.createNamedQuery(Project.FIND_BY_NAME, Project.class)
                .setParameter("name", name)
                .getResultList();
    }

    @Override
    public List<Project> mostDownloaded() {
        return em.createNamedQuery(Project.FETCH_MOST_DOWNLOADED, Project.class)
                .getResultList();
    }

    @Override
    public List<Project> mostRated() {
        return em.createNamedQuery(Project.FETCH_MOST_RATED, Project.class)
                .getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> aggregateLanguageCount() {
        return em.createNamedQuery(Project.AGREGATE_LANGUAGE_COUNT).getResultList();
    }

    @Override
    public List<Project> findByLanguage(String languageName) {
        return em.createNamedQuery(Project.FIND_BY_LANGUAGE_NAME, Project.class)
                .setParameter("languagename", languageName)
                .getResultList();
    }

    @Override
    public List<Project> findByTag(String tagName) {
        return em.createNamedQuery(Project.FIND_BY_TAG, Project.class)
                .setParameter("tagname", tagName)
                .getResultList();
    }

    @Override
    public List<Project> findByQuery(String query) {
        return em.createNamedQuery(Project.SEARCH, Project.class)
                .setParameter("query1", "%" + query + "%")
                .setParameter("query2", "%" + query + "%")
                .getResultList();
    }
}
