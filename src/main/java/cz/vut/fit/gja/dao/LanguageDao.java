package cz.vut.fit.gja.dao;

import cz.vut.fit.gja.entities.Language;

/**
 *
 */
public interface LanguageDao extends AbstractDao<Language> {
}
