package cz.vut.fit.gja.dao;

import java.util.List;

/**
 *
 */
public interface AbstractDao<T> {

    void create(T entity);

    void update(T entity);

    void remove(T entity);

    T find(Object id);

    List<T> findAll();

    List<T> findRange(int[] range);

    int count();

    <T> T getReference(Class<T> entityClass, Object primaryKey);
}
