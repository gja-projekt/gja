
package cz.vut.fit.gja.dao.impl;

import cz.vut.fit.gja.dao.LanguageDao;
import cz.vut.fit.gja.entities.Language;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 *
 */
@Transactional
@Repository
public class LanguageDaoImpl extends AbstractDaoImpl<Language> implements LanguageDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LanguageDaoImpl() {
        super(Language.class);
    }
    
}
