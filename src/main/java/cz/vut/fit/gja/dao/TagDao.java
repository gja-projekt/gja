package cz.vut.fit.gja.dao;

import cz.vut.fit.gja.entities.Tag;

import java.util.List;

/**
 *
 */
public interface TagDao extends AbstractDao<Tag> {

    Tag findByName(String name);

    Integer aggregateMaxRelevance();

    List<Tag> findAllOrderByRelevance(int count);
}
