package cz.vut.fit.gja.controllers;

import cz.vut.fit.gja.Scopes;
import cz.vut.fit.gja.constants.FacesConstants;
import cz.vut.fit.gja.controllers.data.LazyProjectModel;
import cz.vut.fit.gja.controllers.data.ProjectDataFilter;
import cz.vut.fit.gja.controllers.util.TagsAutocompleteBean;
import cz.vut.fit.gja.dto.ProjectDto;
import cz.vut.fit.gja.entities.Language;
import cz.vut.fit.gja.entities.Project;
import cz.vut.fit.gja.services.LanguageService;
import cz.vut.fit.gja.services.ProjectService;
import org.apache.log4j.Logger;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.RateEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static cz.vut.fit.gja.constants.FacesConstants.PROJECT_DETAIL;
import static cz.vut.fit.gja.constants.FacesConstants.REDIRECT;
import cz.vut.fit.gja.utils.FileSystemUtils;
import cz.vut.fit.gja.utils.SourceCodeFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.file.Files;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@Lazy
@Scope(Scopes.VIEW)
@Controller
public class ProjectController extends BaseController implements Serializable {

    private static final Logger logger = Logger.getLogger(ProjectController.class);

    @Autowired
    private ProjectService projectService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private TagsAutocompleteBean autocompleteBean;

    private ProjectDto projectDto;

    private SourceCodeFile selectedSCFile;

    public ProjectController() {
    }

    public void create(ProjectDto projectDto) {
        projectService.create(projectDto);
    }

    public ProjectDto find(String idStr) {
        try {
            int id = Integer.valueOf(idStr);
            ProjectDto projectDto = find(id);
            this.projectDto = projectDto;
            return projectDto;
        } catch (NumberFormatException e) {
            return null;
        }

    }

    public ProjectDto find(int id) {
        if (id == 0) {
            return null;
        }
        ProjectDto projectDto = projectService.find(id);
        return projectDto;
    }

    public void loadProjectBean(int id) {

        ProjectDto dto = find(id);
        if (dto != null) {
            projectDto = dto;
        }
    }

    public void load(ComponentSystemEvent event) {
        String projectId = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get(FacesConstants.PARAM_PROJECT_ID);
        int id;
        try {
            id = Integer.valueOf(projectId);
        } catch (NumberFormatException e) {
            id = 0;
        }

        loadProjectBean(id);
    }

    public List<ProjectDto> findAll() {
        return projectService.findAll();
    }

    public List<ProjectDto> findAllByFilter() {
        ProjectDataFilter filter = (ProjectDataFilter) getSessionMap().get(FacesConstants.FILTER);

        if (filter != null && filter.getLanguageName() != null) {
            return projectService.findByLanguage(filter.getLanguageName());
        }

        if (filter != null && filter.getTagName() != null) {
            return projectService.findByTag(filter.getTagName());
        }

        return Collections.emptyList();
    }

    public boolean isFiltered() {
        ProjectDataFilter filter = (ProjectDataFilter) getSessionMap().get(FacesConstants.FILTER);

        return filter != null && (filter.getLanguageName() != null || filter.getTagName() != null);
    }

    public LazyProjectModel getLazyModel() {
        return new LazyProjectModel(projectService.findAll());
    }

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    protected void linkWithTags(ProjectDto projectDto) {
        projectService.linkWithTags(projectDto);
    }

    public boolean isOneFile() {
        File projectDataStore = FileSystemUtils.getProjectDataStore(projectDto.getId());
        File[] list = projectDataStore.listFiles();
        return (list != null && list.length == 1);
    }

    public boolean isMoreFiles() {
        File projectDataStore = FileSystemUtils.getProjectDataStore(projectDto.getId());
        File[] list = projectDataStore.listFiles();
        return (list != null && list.length > 1);
    }

    public String getFilesSourceCode(File name) {
        String code = "";
        try {
            CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
            decoder.onMalformedInput(CodingErrorAction.IGNORE);
            InputStreamReader sreader = new InputStreamReader(new FileInputStream(name), decoder);
            BufferedReader reader = new BufferedReader(sreader);
            String line;
            while ((line = reader.readLine()) != null) {
                code += line + "\n";
            }
        } catch (IOException ex) {
            return "An error while opening file occured! We are sorry.";
        }

        return code;
    }

    public String getSourceCodeFromFile(File file) {
        if (file.isFile()) {
            String code = "";
            try {
                CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
                decoder.onMalformedInput(CodingErrorAction.IGNORE);
                InputStreamReader sreader = new InputStreamReader(new FileInputStream(file), decoder);
                BufferedReader reader = new BufferedReader(sreader);
                String line;
                while ((line = reader.readLine()) != null) {
                    code += line + "\n";
                }
            } catch (IOException ex) {
                return "An error while opening file occured! We are sorry.";
            }

            return code;
        }
        return "An error occured, file " + file.getName() + " is not file!";
    }

    public String getSourceCode() {
        File projectDataStore = FileSystemUtils.getProjectDataStore(projectDto.getId());
        File[] list = projectDataStore.listFiles();

        if (list[0].isFile()) {
            String code = "";
            try {
                CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
                decoder.onMalformedInput(CodingErrorAction.IGNORE);
                InputStreamReader sreader = new InputStreamReader(new FileInputStream(list[0]), decoder);
                BufferedReader reader = new BufferedReader(sreader);

                String line;
                while ((line = reader.readLine()) != null) {
                    code += line + "\n";
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                return "An error while opening file occured! We are sorry. " + ex.getMessage();
            }

            return code;
        }
        return "An error occured, file " + list[0].getName() + " is not file!";
    }

    public TreeNode getRoot() {
        TreeNode root = new DefaultTreeNode("root", null);
        root.setExpanded(true);
        root.setSelectable(false);

        if (projectDto != null) {
            File projectDataStore = FileSystemUtils.getProjectDataStore(projectDto.getId());
            getFolderContent(projectDataStore, root);
        }
        return root;
    }

    private void getFolderContent(File folder, TreeNode parentNode) {
        File[] list = folder.listFiles();

        for (File file : list) {
            if (file.isFile()) {
                TreeNode node = new DefaultTreeNode(new SourceCodeFile(file.getName(), FilenameUtils.getExtension(file.getName()), file.length(), file), parentNode);
                node.setExpanded(true);
                node.setSelectable(false);
            } else {
                TreeNode node = new DefaultTreeNode(new SourceCodeFile(file.getName(), "Folder", 0), parentNode);
                node.setExpanded(true);
                node.setSelectable(false);
                getFolderContent(file, node);
            }
        }
    }

    public SourceCodeFile getSelectedSCFile() {
        logger.debug("getSelectedSCFile");
        return selectedSCFile;
    }

    public void setSelectedSCFile(SourceCodeFile selectedSCFile) {
        logger.debug("setSelectedSCFile");
        this.selectedSCFile = selectedSCFile;
    }

    public List<String> getLanguages() {
        return languageNames(languageService.findAll());
    }

    public List<SelectItem> getLanguagesSelectItems() {
        return getLanguageItems(languageService.findAll());
    }

    private List<SelectItem> getLanguageItems(List<Language> all) {
        ArrayList<SelectItem> langs = new ArrayList<>();
        for (Language language : all) {
            langs.add(new SelectItem(language.getId(), language.getName()));
        }
        return langs;
    }

    private List<String> languageNames(List<Language> all) {
        ArrayList<String> langs = new ArrayList<>();
        for (Language language : all) {
            langs.add(language.getName());
        }
        return langs;
    }

    public void onRate(RateEvent rate) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your rated has been accepted!", "You rated: " + rate.getRating() + " stars");
        FacesContext.getCurrentInstance().addMessage(null, message);

        logger.info("onRate");
        projectDto.setVotes(projectDto.getVotes() + 1);
        projectDto.setRating((short) (projectDto.getRating() + Integer.valueOf(rate.getRating().toString())));

        projectService.update(projectDto);

        // refreshPage(projectDto);
        // addMessage(FacesMessage.SEVERITY_INFO, "Rate Project", "You rated: " + rate.getRating());
    }

    protected void refreshPage(ProjectDto projectDto) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI() + '?' + FacesConstants.PARAM_PROJECT_ID + "=" + projectDto.getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public StreamedContent getDownload() {
        logger.info("Download");

        projectDto.setDownloadCount(projectDto.getDownloadCount() + 1);
        projectService.update(projectDto);
        return new DefaultStreamedContent(projectService.getZipProject(projectDto.getId()), "application/zip", projectDto.getTitle() + ".zip");
    }

    public static String redirectToProjectUrl(String id) {
        return String.format("%s%s&%s=%s", PROJECT_DETAIL, REDIRECT, FacesConstants.PARAM_PROJECT_ID, id);
    }

    public static void redirectToProject(String id) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String outcome = redirectToProjectUrl(id);
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public ProjectService getService() {
        return projectService;
    }

    public int getRating() {
        return (int) (projectDto.getRating() / (double) projectDto.getVotes());
    }

    public int getVotes() {
        return projectDto.getVotes();
    }

    @FacesConverter(forClass = Project.class)
    public static class ProjectControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProjectController controller = (ProjectController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "projectController");
            return controller.getService().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Project) {
                Project o = (Project) object;
                return getStringKey(o.getId());
            } else {
                logger.info(String.format("object %s is of type %s; expected type: %s", object.toString(), object.getClass().getName(), Project.class.getName()));
                return null;
            }
        }

    }

    public TagsAutocompleteBean getAutocompleteBean() {
        return autocompleteBean;
    }

    public void setAutocompleteBean(TagsAutocompleteBean autocompleteBean) {
        this.autocompleteBean = autocompleteBean;
    }

    public void setLanguageService(LanguageService languageService) {
        this.languageService = languageService;
    }

    public ProjectDto getProjectDto() {
        return projectDto;
    }

    public void setProjectDto(ProjectDto projectDto) {
        this.projectDto = projectDto;
    }
}
