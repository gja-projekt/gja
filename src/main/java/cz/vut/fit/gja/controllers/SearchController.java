package cz.vut.fit.gja.controllers;

import cz.vut.fit.gja.Scopes;
import cz.vut.fit.gja.constants.FacesConstants;
import cz.vut.fit.gja.dto.ProjectDto;
import cz.vut.fit.gja.dto.SearchDto;
import cz.vut.fit.gja.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import static cz.vut.fit.gja.constants.FacesConstants.PROJECT_DETAIL_XHTML;
import static cz.vut.fit.gja.constants.FacesConstants.REDIRECT;

@Lazy
@Controller
@Scope(Scopes.SESSION)
public class SearchController extends BaseController implements Serializable {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private SearchDto searchDto;

    public SearchController() {
    }

    public String search() {
        getSessionMap().put(FacesConstants.QUERY, searchDto.getQuery());
        searchDto.setQuery(null);

        return "/pages/search/result.xhtml" + FacesConstants.REDIRECT;
    }

    public String detailUrl(String id) {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        return String.format("%s%s%s&%s=%s", request.getContextPath() + "/faces", PROJECT_DETAIL_XHTML, REDIRECT, FacesConstants.PARAM_PROJECT_ID, id);
    }

    public SearchDto getSearchDto() {
        return searchDto;
    }

    public void setSearchDto(SearchDto searchDto) {
        this.searchDto = searchDto;
    }

    public String getQuery() {
        return searchDto.getQuery();
    }

    public String getQueryForHighlight() {
        return (String) getSessionMap().get(FacesConstants.QUERY);
    }

    public List<ProjectDto> findAllByQuery() {
        String query = (String) getSessionMap().get(FacesConstants.QUERY);
        //getSessionMap().remove(FacesConstants.QUERY);

        if (query != null && !query.isEmpty()) {
            List<ProjectDto> results = projectService.findByTag(query);

            if (results.isEmpty()) {
                results = projectService.findByQuery(query);
            }
            return results;
        }

        return Collections.emptyList();
    }

}
