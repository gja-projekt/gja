package cz.vut.fit.gja.controllers;

import cz.vut.fit.gja.constants.FacesConstants;
import cz.vut.fit.gja.dao.LanguageDao;
import cz.vut.fit.gja.dto.ProjectDto;
import cz.vut.fit.gja.services.ProjectService;
import cz.vut.fit.gja.utils.UploadedZip;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.IOException;

import static cz.vut.fit.gja.constants.FacesConstants.PROJECT_DETAIL;
import static cz.vut.fit.gja.constants.FacesConstants.REDIRECT;

/**
 *
 */
@Lazy
@Controller
public class UploadCodeController extends ProjectController {

    private static Logger logger = Logger.getLogger(UploadCodeController.class);

    @Autowired
    private ProjectDto projectDto;

    @Autowired
    private LanguageDao languageDao;

    @Autowired
    private ProjectService projectService;

    public String uploadFile(FileUploadEvent event) {
        logger.debug("uploadCodeController#uploadFile()");
        logger.debug("Uploading " + FilenameUtils.getName(event.getFile().getFileName()));

        saveFile(event.getFile());

        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        redirectToProject();
        return redirectToProjectUrl();
    }

    public String uploadZipFile(FileUploadEvent event) {
        logger.debug("uploadCodeController#uploadZipFile()");
        logger.debug("Uploading " + FilenameUtils.getName(event.getFile().getFileName()));

        UploadedZip uploadedZip = null;
        try {
            uploadedZip = new UploadedZip(event.getFile());
            projectService.saveZipFile(uploadedZip, projectDto);
        } catch (IOException e) {
            logger.error(e);
        }

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        redirectToProject();
        return redirectToProjectUrl();
    }

    private void saveFile(UploadedFile file) {
        String name = FilenameUtils.getName(file.getFileName());
        byte[] data = file.getContents();

        projectService.saveFile(name, data, projectDto);
    }

    public String redirectToProjectUrl() {
        if (projectDto.getId() != null)
            return String.format("%s?%s&%s=%s", PROJECT_DETAIL, REDIRECT, FacesConstants.PARAM_PROJECT_ID, Integer.toString(projectDto.getId()));
        return null;
    }

    public void redirectToProject() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String outcome = redirectToProjectUrl();
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
    }

    public ProjectDto getProjectDto() {
        return projectDto;
    }

    public void setProjectDto(ProjectDto projectDto) {
        this.projectDto = projectDto;
    }

    public LanguageDao getLanguageDao() {
        return languageDao;
    }

    public void setLanguageDao(LanguageDao languageDao) {
        this.languageDao = languageDao;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }
}
