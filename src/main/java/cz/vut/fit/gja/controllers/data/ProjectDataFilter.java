package cz.vut.fit.gja.controllers.data;

/**
 *
 */
public class ProjectDataFilter {

    private String languageName;
    private String tagName;

    public ProjectDataFilter() {
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
    
    public String getTagName() {
        return tagName;
    }
    
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public void clear() {
        languageName = null;
        tagName = null;
    }
}
