package cz.vut.fit.gja.controllers;

import org.springframework.context.annotation.Lazy;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.Map;

/**
 *
 */
@Lazy
public class BaseController {

    protected Map<String, Object> getRequestMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
    }

    protected Map<String, Object> getSessionMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

    protected Map<String, Object> getViewMap() {
        return FacesContext.getCurrentInstance().getViewRoot().getViewMap();
    }

    protected void invalidate() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

    protected void addMessage(FacesMessage.Severity severity, String title, String message) {
        FacesMessage m = new FacesMessage(severity, title, message);
        FacesContext.getCurrentInstance().addMessage(null, m);
    }

    protected Object get(String key) {
        return getSessionMap().get(key);
    }

    protected void put(String key, Object value) {
        getSessionMap().put(key, value);
    }
}
