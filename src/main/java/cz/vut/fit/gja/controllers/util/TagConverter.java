package cz.vut.fit.gja.controllers.util;

import cz.vut.fit.gja.dto.TagDto;
import cz.vut.fit.gja.entities.Tag;
import cz.vut.fit.gja.dao.TagDao;
import cz.vut.fit.gja.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;


@Component
public class TagConverter implements Converter {
    
    @Autowired
    private TagDao tagDao;
    
    @Autowired
    private TagService tagService;
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String submittedValue) {
        if (submittedValue.trim().isEmpty()) {
            return null;
        } else {
            try {
                Tag tag = tagDao.findByName(submittedValue);
                if (tag == null){
                    tag = new Tag();
                    tag.setId(0);
                    tag.setName(submittedValue);
                    tag.setRelevance(0);
                }
                return tagService.toDto(tag);
            } catch (NumberFormatException exception) {
                throw new ConverterException(
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid tag"));
            }
        }
    }
    
    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return ((TagDto) value).getName();
        }
    }
}
