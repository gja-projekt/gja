package cz.vut.fit.gja.controllers;

import cz.vut.fit.gja.constants.FacesConstants;
import cz.vut.fit.gja.dao.LanguageDao;
import cz.vut.fit.gja.dto.ProjectDto;
import cz.vut.fit.gja.services.ProjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import static cz.vut.fit.gja.constants.FacesConstants.PROJECT_DETAIL;
import static cz.vut.fit.gja.constants.FacesConstants.REDIRECT;
/**
 *
 */
@Lazy
@Controller
public class PasteCodeController extends ProjectController {

    private static Logger logger = Logger.getLogger(PasteCodeController.class.getName());

    @Autowired
    private ProjectDto projectDto;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private LanguageDao languageDao;

    public String submit() {
        logger.debug("pasteCodeController#submit()");
        linkWithTags(projectDto);
        create(projectDto);
        savePasteCode(projectDto);
        invalidate();
        return String.format("%s?%s&%s=%s", PROJECT_DETAIL, REDIRECT, FacesConstants.PARAM_PROJECT_ID, Integer.toString(projectDto.getId()));
    }

    private void savePasteCode(ProjectDto projectDto) {
        projectService.saveCode(projectDto);
    }

    public ProjectDto getProjectDto() {
        return projectDto;
    }

    public void setProjectDto(ProjectDto projectDto) {
        this.projectDto = projectDto;
    }

    public LanguageDao getLanguageDao() {
        return languageDao;
    }

    public void setLanguageDao(LanguageDao languageDao) {
        this.languageDao = languageDao;
    }

}
