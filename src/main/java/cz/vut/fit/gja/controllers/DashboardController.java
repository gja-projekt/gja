package cz.vut.fit.gja.controllers;

import cz.vut.fit.gja.constants.DashboardConstants;
import cz.vut.fit.gja.constants.FacesConstants;
import cz.vut.fit.gja.controllers.data.ProjectDataFilter;
import cz.vut.fit.gja.dto.ProjectDto;
import cz.vut.fit.gja.entities.Language;
import cz.vut.fit.gja.entities.Tag;
import cz.vut.fit.gja.services.LanguageService;
import cz.vut.fit.gja.services.ProjectService;
import cz.vut.fit.gja.services.TagService;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.tagcloud.DefaultTagCloudItem;
import org.primefaces.model.tagcloud.DefaultTagCloudModel;
import org.primefaces.model.tagcloud.TagCloudItem;
import org.primefaces.model.tagcloud.TagCloudModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 *
 */
@Lazy
@Controller
public class DashboardController extends BaseController {

    private DashboardModel dashboardModel;
    private TagCloudModel tagCloudModel;
    private TagCloudModel languageCloudModel;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private LanguageService languageService;
    
    @Autowired
    private TagService tagService;

    @PostConstruct
    void init() {
        dashboardModel = new DefaultDashboardModel() {{
            addColumn(new DefaultDashboardColumn() {{
                addWidget(DashboardConstants.TAG_CLOUD);
                addWidget(DashboardConstants.LANGUAGE_CLOUD);
            }});
            addColumn(new DefaultDashboardColumn() {{
                addWidget(DashboardConstants.TOP_RATED);
                addWidget(DashboardConstants.TOP_DOWNLOADED);
            }});
        }};
    }

    private void initTagCloudModel() {
        tagCloudModel = new DefaultTagCloudModel();

        Double max = tagService.aggregateMaxRelevance().doubleValue();
        for (Tag tag : tagService.findAll()) {
            DefaultTagCloudItem item = createTagCloudItem(tag.getName(), (long) tag.getRelevance(), max);
            
            tagCloudModel.addTag(item);
        }
    }

    private void initLanguageCloudModel() {
        languageCloudModel = new DefaultTagCloudModel();

        Double max = null;
        for (Object[] dto : projectService.aggregateLanguageCount()) {
            Language language = (Language) dto[0];
            Long count = (Long) dto[1];

            if (count == 0) {
                continue;
            }

            if (max == null) {
                max = count.doubleValue();
            }

            DefaultTagCloudItem item = createTagCloudItem(language.getName(), count, max);

            languageCloudModel.addTag(item);
        }

    }

    private DefaultTagCloudItem createTagCloudItem(String label, Long count, Double max) {
        // remaping to values 1-5 for tag cloud component
        int strength = (int) ((count.intValue() / max) * 5);
        if (strength == 0) {
            strength++;
        }
        return new DefaultTagCloudItem(label, strength);
    }

    public String detail(String id) {
        ProjectController.redirectToProjectUrl(id);
        return ProjectController.redirectToProjectUrl(id);
    }

    public void onLanguageSelect(SelectEvent event) {
        TagCloudItem item = (TagCloudItem) event.getObject();

        ProjectDataFilter projectDataFilter = new ProjectDataFilter();
        projectDataFilter.setLanguageName(item.getLabel());
        getSessionMap().put(FacesConstants.FILTER, projectDataFilter);

        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Language selected", item.getLabel());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onTagSelect(SelectEvent event) {
        TagCloudItem item = (TagCloudItem) event.getObject();

        ProjectDataFilter projectDataFilter = new ProjectDataFilter();
        projectDataFilter.setTagName(item.getLabel());
        getSessionMap().put(FacesConstants.FILTER, projectDataFilter);

        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Tag selected", item.getLabel());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public DashboardModel getDashboardModel() {
        return dashboardModel;
    }

    public void setDashboardModel(DashboardModel dashboardModel) {
        this.dashboardModel = dashboardModel;
    }

    public TagCloudModel getTagCloudModel() {
        if (tagCloudModel == null)
            initTagCloudModel();
        return tagCloudModel;
    }

    public void setTagCloudModel(TagCloudModel tagCloudModel) {
        this.tagCloudModel = tagCloudModel;
    }

    public TagCloudModel getLanguageCloudModel() {
        if (languageCloudModel == null)
            initLanguageCloudModel();
        return languageCloudModel;
    }

    public void setLanguageCloudModel(TagCloudModel languageCloudModel) {
        this.languageCloudModel = languageCloudModel;
    }

    public List<ProjectDto> getMostDownloaded() {
        return projectService.getMostDownloaded();
    }

    public List<ProjectDto> getMostRated() {
        return projectService.getMostRated();
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void setLanguageService(LanguageService languageService) {
        this.languageService = languageService;
    }

}
