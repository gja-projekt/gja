package cz.vut.fit.gja.controllers.util;

import cz.vut.fit.gja.Scopes;
import cz.vut.fit.gja.dao.TagDao;
import cz.vut.fit.gja.dto.TagDto;
import cz.vut.fit.gja.entities.Tag;
import cz.vut.fit.gja.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Lazy
@Scope(Scopes.VIEW)
@Component
public class TagsAutocompleteBean implements Serializable {

    private List<Tag> tags = null;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private TagService tagService;

    //@Value("${suggestion.limit}")
    @Value("10")
    private int limit;

    public TagsAutocompleteBean() {
    }

    public List<TagDto> suggestion(final String query) {
        init();

        final List<TagDto> results = new ArrayList<TagDto>();

        for (Tag tag : tags) {
            if (tag.getName().toLowerCase().startsWith(query.toLowerCase())) {
                results.add(tagService.toDto(tag));
            }
        }

        if (!results.contains(new TagDto(){{setName(query);}}) && !query.isEmpty()) {
            TagDto t = new TagDto();
            t.setName(query);
            t.setId(0);
            results.add(0, t);
        }

        return results;
    }

    private void init() {
        if (tags == null)
            tags = tagDao.findAllOrderByRelevance(getLimit());
    }

    public TagDao getTagDao() {
        return tagDao;
    }

    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }

    public TagService getTagService() {
        return tagService;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
