package cz.vut.fit.gja.controllers.data;

import cz.vut.fit.gja.dto.ProjectDto;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.*;

/**
 *
 */
public class LazyProjectModel extends LazyDataModel<ProjectDto> {

    private List<ProjectDto> datasource;

    public LazyProjectModel(List<ProjectDto> datasource) {
        this.datasource = datasource;
    }

    @Override
    public ProjectDto getRowData(String rowKey) {
        for(ProjectDto p : datasource) {
            if(p.getId().toString().equals(rowKey))
                return p;
        }
        return null;
    }

    @Override
    public Object getRowKey(ProjectDto p) {
        return p.getId();
    }

    @Override
    public List<ProjectDto> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,String> filters) {
        List<ProjectDto> data = new ArrayList<ProjectDto>();

        //filter
        for(ProjectDto car : datasource) {
            boolean match = true;

            for(Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                try {
                    String filterProperty = it.next();
                    String filterValue = filters.get(filterProperty);
                    String fieldValue = String.valueOf(car.getClass().getField(filterProperty).get(car));

                    if(filterValue == null || fieldValue.startsWith(filterValue)) {
                        match = true;
                    }
                    else {
                        match = false;
                        break;
                    }
                } catch(Exception e) {
                    match = false;
                }
            }

            if(match) {
                data.add(car);
            }
        }

        //sort
        if(sortField != null) {
            Collections.sort(data, new LazySorter(sortField, sortOrder));
        }

        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);

        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }

    protected class LazySorter implements Comparator<ProjectDto> {
        public LazySorter(String sortField, SortOrder sortOrder) {
        }

        @Override
        public int compare(ProjectDto o1, ProjectDto o2) {
            return 0;
        }
    }
}
