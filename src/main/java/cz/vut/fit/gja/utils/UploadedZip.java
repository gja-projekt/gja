package cz.vut.fit.gja.utils;

import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.primefaces.model.UploadedFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 */
public class UploadedZip extends ZipArchiveInputStream {

    private UploadedFile zip;

    public UploadedZip(UploadedFile zip) throws IOException {
        super(zip.getInputstream());
    }

    public void copy(File target) throws IOException {
        int len;
        byte[] buffer = new byte[1024];
        FileOutputStream fos = new FileOutputStream(target);
        try {
            while ((len = read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
        }
        finally {
            if (fos != null) {
                fos.close();
            }
        }
    }
}
