package cz.vut.fit.gja.utils;

import java.io.File;

public class SourceCodeFile {

    private String type;
    private String name;
    private double size;
    private File file;

    public SourceCodeFile(String name, String type, double size) {
        this.type = type;
        this.name = name;
        this.size = size;
    }

    public SourceCodeFile(String name, String type, double size, File file) {
        this.type = type;
        this.name = name;
        this.size = size;
        this.file = file;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String getSizeInKb() {
        return String.format("%.2f%n", getSize() / 1024);
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
