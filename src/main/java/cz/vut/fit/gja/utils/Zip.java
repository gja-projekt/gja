package cz.vut.fit.gja.utils;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/*
 * http://www.mkyong.com/java/how-to-compress-files-in-zip-format/
 */
public class Zip {

    private final Logger logger = Logger.getLogger(Zip.class);

    List<String> fileList;
    private final String output;
    private final String sourceDir;

    public Zip(String sourceDir, String output){
        fileList = new ArrayList<String>();
        this.output = output;
        this.sourceDir = sourceDir;
        generateFileList(new File(sourceDir));
    }

    /**
     * Zip it
     */
    public boolean zipIt() throws IOException {
        String zipFile = output;
        if (fileList.isEmpty())
            return false;

        byte[] buffer = new byte[1024];

        try{

            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);

            logger.debug("Output to Zip : " + zipFile);

            for(String file : this.fileList){

                logger.debug("File Added : " + file);
                ZipEntry ze= new ZipEntry(file);
                zos.putNextEntry(ze);

                FileInputStream in =
                        new FileInputStream(sourceDir + File.separator + file);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zos.closeEntry();
            //remember close it
            zos.close();

            logger.debug("Done");
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        return true;
    }

    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     * @param node file or directory
     */
    public void generateFileList(File node){

        //add file only
        if(node.isFile()){
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
        }

        if(node.isDirectory()){
            String[] subNote = node.list();
            for(String filename : subNote){
                generateFileList(new File(node, filename));
            }
        }

    }

    /**
     * Format the file path for zip
     * @param file file path
     * @return Formatted file path
     */
    private String generateZipEntry(String file){
        return file.substring(sourceDir.length()+1, file.length());
    }
}