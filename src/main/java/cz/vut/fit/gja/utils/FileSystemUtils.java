package cz.vut.fit.gja.utils;

import org.apache.http.util.EncodingUtils;

import java.io.File;

/**
 *
 */
public class FileSystemUtils {

    public static final String JBOSS_BIN_PATH = System.getProperty("user.dir");
    public static final File JBOSS_BIN = new File(JBOSS_BIN_PATH);

    public static final File JBOSS_HOME = JBOSS_BIN.getParentFile();

    public static final String DATA_STORE_DIR = "standalone/projects";

    public static final File getProjectsDataStore() {
        File projects = new File(JBOSS_HOME, DATA_STORE_DIR);
        if (!projects.exists()) {
            if (!projects.mkdir()) {
                throw new RuntimeException("Can't create data storage directory");
            }
        }
        return projects;
    }

    public static final File getProjectDataStore(int id) {
        File p = new File(getProjectsDataStore(), Integer.valueOf(id).toString());
        if (!p.exists()) {
            if (!p.mkdir()) {
                throw new RuntimeException("Can't create data storage directory");
            }
        }
        return p;
    }

    public static String asciiName(String name) {
        return EncodingUtils.getAsciiString(name.getBytes());
    }
}
