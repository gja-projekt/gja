package cz.vut.fit.gja.dto;

import cz.vut.fit.gja.Scopes;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 *
 */
@Scope(Scopes.SESSION)
@Component
public class SearchDto implements Serializable {

    public static final String BEAN_NAME = "searchDto";

    private String query;

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }
}
