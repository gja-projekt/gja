package cz.vut.fit.gja.dto;

import cz.vut.fit.gja.Scopes;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 *
 */
@Scope(Scopes.VIEW)
@Component
public class LanguageDto implements Serializable {
    private Integer id;
    private String name;
    private String highlightMark;
    private String extension;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setHighlightMark(String highlightMark) {
        this.highlightMark = highlightMark;
    }

    public String getHighlightMark() {
        return highlightMark;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }
}
