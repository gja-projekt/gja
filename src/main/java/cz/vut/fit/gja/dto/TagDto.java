package cz.vut.fit.gja.dto;

import cz.vut.fit.gja.Scopes;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 *
 */
@Scope(Scopes.VIEW)
@Component
public class TagDto implements Serializable {
    private int id;
    private String name;
    private int relevance;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRelevance(int relevance) {
        this.relevance = relevance;
    }

    public int getRelevance() {
        return relevance;
    }
    
    @Override
    public String toString(){
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !o.getClass().isAssignableFrom(getClass())) return false;

        TagDto tagDto = (TagDto) o;

        if (!name.equals(tagDto.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
