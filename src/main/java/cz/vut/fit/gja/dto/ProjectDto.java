package cz.vut.fit.gja.dto;

import cz.vut.fit.gja.Scopes;
import cz.vut.fit.gja.constants.FacesConstants;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 */
@Scope(Scopes.SESSION)
@Component
public class ProjectDto implements Serializable {
    
    public static final String BEAN_NAME = "projectDto";
    
    private Integer id;
    private String code = "";
    private String language;
    private LanguageDto languageDto;
    private String title = "";
    private Date expiration;
    @Size(max = FacesConstants.DESCRIPTION_LENGTH)
    private String description;
    private List<TagDto> selectedTags = Collections.emptyList();
    private Date created;
    private int downloadCount;
    private short rating;
    private int votes;
    private List<TagDto> tagList = Collections.emptyList();
    
    public Integer getId() {
        Logger logger = Logger.getLogger("projectDto");
        logger.info("getId() = " + id);
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getCode() {
        return code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getLanguage() {
        return language;
    }
    
    public void setLanguage(String language) {
        this.language = language;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getTitleHighligted(String query) {
        return title.replaceAll(query, "<span style='background-color: #ffff00;'>" + query + "</span>");
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getStringExpiration() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return (expiration != null ? dateFormat.format(expiration) : "<em>unknown</em>");
    }
    
    public String getStringCreated() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return (created != null ? dateFormat.format(created) : "<em>unknown</em>");
    }
    
    public Date getExpiration() {
        return expiration;
    }
    
    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
    
    public boolean isValidFileUpload() {
        return language != null && title != null;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
    public String getDescriptionHighlighted(String query) {
        return getDescription().replaceAll(query, "<span style='background-color: #ffff00;'>" + query + "</span>");
    }
    
    public void setSelectedTags(List<TagDto> selectedTags) {
        if (selectedTags != null) {
            this.selectedTags = selectedTags;
        } else {
            this.selectedTags = Collections.emptyList();
        }
    }
    
    public List<TagDto> getSelectedTags() {
        return selectedTags;
    }
    
    public String getTags() {
        List<TagDto> list = getTagList();
        if (list.isEmpty()) {
            return "<em>no tags specified</em>";
        }
        
        String ret = list.get(0).getName();
        if (list.size() > 1) {
            for (int i = 1; i < list.size(); i++) {
                TagDto tagDto = list.get(i);
                ret += ", " + tagDto.getName();
            }
        }
        return ret;
    }
    
    public void setCreated(Date created) {
        this.created = created;
    }
    
    public Date getCreated() {
        return created;
    }
    
    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }
    
    public int getDownloadCount() {
        return downloadCount;
    }
    
    public void setRating(short rating) {
        this.rating = rating;
    }
    
    public short getRating() {
        return rating;
    }
    
    public int getVotes() {
        return votes;
    }
    
    public void setVotes(int votes) {
        this.votes = votes;
    }
    
    public void setTagList(List<TagDto> tagList) {
        this.tagList = tagList;
    }
    
    public List<TagDto> getTagList() {
        return tagList;
    }
    
    public LanguageDto getLanguageDto() {
        return languageDto;
    }
    
    public void setLanguageDto(LanguageDto languageDto) {
        this.languageDto = languageDto;
    }
}
