package cz.vut.fit.gja.constants;

import org.springframework.stereotype.Component;

import javax.faces.bean.ApplicationScoped;

/**
 *
 */
@Component("FacesConstants")
@ApplicationScoped
public class FacesConstants {
    public static final String PASTE_CODE_PAGE = "index";
    public static final String PROJECT_DETAIL = "/pages/project/Detail";
    public static final String PROJECT_DETAIL_XHTML = "/pages/project/Detail.xhtml";
    public static final String UPLOAD_FILE_PAGE = "uploadFilesStep2";
    public static final String UPLOAD_ZIP_FILE_PAGE = "uploadZipStep2";

    public static final int DESCRIPTION_LENGTH = 10000;
    public static final int TAG_LENGTH = 100;

    public static final String FILTER = "filter";
    
    public static final String QUERY = "query";

    public static final String PARAM_PROJECT_ID = "projectId";

    public static final String REDIRECT = "?faces-redirect=true";

    public int getDESCRIPTION_LENGTH() {
        return DESCRIPTION_LENGTH;
    }

    public int getTAG_LENGTH() {
        return TAG_LENGTH;
    }

    public String getREDIRECT() {
        return REDIRECT;
    }
}
