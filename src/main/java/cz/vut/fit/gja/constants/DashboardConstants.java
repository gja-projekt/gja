package cz.vut.fit.gja.constants;

import org.springframework.stereotype.Component;

import javax.faces.bean.ApplicationScoped;

/**
 *
 */
@Component("DashboardConstants")
@ApplicationScoped
public class DashboardConstants {

    public static final String TAG_CLOUD = "tagCloud";
    public static final String LANGUAGE_CLOUD = "languageCloud";
    public static final String TOP_DOWNLOADED = "topDownloaded";
    public static final String TOP_RATED = "topRated";

    public String getTAG_CLOUD() {
        return TAG_CLOUD;
    }

    public String getTOP_DOWNLOADED() {
        return TOP_DOWNLOADED;
    }

    public String getTOP_RATED() {
        return TOP_RATED;
    }

    public String getLANGUAGE_CLOUD() {
        return LANGUAGE_CLOUD;
    }
}
