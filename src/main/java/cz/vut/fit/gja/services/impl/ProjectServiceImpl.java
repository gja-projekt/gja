package cz.vut.fit.gja.services.impl;

import cz.vut.fit.gja.dto.ProjectDto;
import cz.vut.fit.gja.dto.TagDto;
import cz.vut.fit.gja.entities.Language;
import cz.vut.fit.gja.entities.Project;
import cz.vut.fit.gja.dao.ProjectDao;
import cz.vut.fit.gja.services.LanguageService;
import cz.vut.fit.gja.services.ProjectService;
import cz.vut.fit.gja.services.TagService;
import cz.vut.fit.gja.utils.FileSystemUtils;
import cz.vut.fit.gja.utils.UploadedZip;
import cz.vut.fit.gja.utils.Zip;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Transactional
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectDao projectDao;

    @Autowired
    TagService tagService;

    @Autowired
    LanguageService languageService;

    @Override
    public void create(ProjectDto projectDto) {
        Project p = toEntity(projectDto);
        projectDao.create(p);
        projectDto.setId(p.getId());
    }

    @Override
    @Transactional
    public ProjectDto find(int id) {
        return toDto(projectDao.find(id));
    }

    @Override
    public ProjectDto toDto(Project project) {
        if (project == null) {
            return null;
        }

        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setTitle(project.getName());
        projectDto.setLanguageDto(languageService.toDto(project.getLanguage()));
        projectDto.setCreated(project.getCreated());
        projectDto.setExpiration(project.getExpire());
        projectDto.setDescription(project.getDescription());
        projectDto.setDownloadCount(project.getDownloadCount());
        projectDto.setRating(project.getRating());
        projectDto.setVotes(project.getVotes());
        projectDto.setTagList(tagService.toDtos(project.getTagList()));
        return projectDto;
    }

    @Override
    public Project toEntity(ProjectDto projectDto) {
        Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getTitle());
        project.setCreated(projectDto.getCreated());
        project.setExpire(projectDto.getExpiration());
        project.setDescription(projectDto.getDescription());
        project.setDownloadCount(projectDto.getDownloadCount());
        project.setRating(projectDto.getRating());
        project.setVotes(projectDto.getVotes());
        project.setTagList(tagService.toEntities(projectDto.getTagList()));
        if (projectDto.getLanguage() != null) {
            project.setLanguage(projectDao.getReference(Language.class, Integer.valueOf(projectDto.getLanguage())));
        } else if (projectDto.getLanguageDto() != null) {
            project.setLanguage(projectDao.getReference(Language.class, projectDto.getLanguageDto().getId()));
        }
        return project;
    }

    private List<ProjectDto> toDtos(List<Project> all) {
        List<ProjectDto> dtos = new ArrayList<ProjectDto>();
        for (Project project : all) {
            dtos.add(toDto(project));
        }
        return dtos;
    }

    @Override
    public ProjectDao getProjectDao() {
        return projectDao;
    }

    public void setProjectDao(ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    @Override
    public void saveCode(ProjectDto dto) {
        saveFile(dto.getTitle(), dto.getCode().getBytes(), dto);
    }

    @Override
    public void saveFile(String fileName, byte[] fileData, ProjectDto dto) {
        if (dto.getId() == null) { //
            linkWithTags(dto);
            create(dto);
        } else { // existing
            find(dto.getId());
        }

        saveFile(fileName, fileData, FileSystemUtils.getProjectDataStore(dto.getId()));
    }

    @Override
    public void saveFile(String fileName, byte[] fileData, File dataStore) {
        File file = new File(dataStore, fileName);

        try {
            FileOutputStream os = new FileOutputStream(file);
            os.write(fileData);
            os.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void saveZipFile(UploadedZip uploadedZip, ProjectDto dto) throws IOException {

        if (dto.getId() == null) { // new
            linkWithTags(dto);
            create(dto);
        } else { // existing
            find(dto.getId());
        }
        saveZipFileEntries(uploadedZip, FileSystemUtils.getProjectDataStore(dto.getId()));
    }

    @Override
    public void saveZipFileEntries(UploadedZip uploadedZip, File dataStore) throws IOException {
        try {
            ArchiveEntry nextEntry;
            while ((nextEntry = uploadedZip.getNextZipEntry()) != null) {
                File target = new File(dataStore, FileSystemUtils.asciiName(nextEntry.getName()));
                target.getParentFile().mkdirs();

                if (nextEntry.isDirectory()) {
                    continue;
                }

                uploadedZip.copy(target);
            }
        } finally {
            uploadedZip.close();
        }
    }

    @Override
    public void linkWithTags(ProjectDto projectDto) {
        ArrayList<TagDto> tagDtos = new ArrayList<TagDto>();

        for (TagDto tag : projectDto.getSelectedTags()) {
            TagDto tagDto = tagService.findByName(tag.getName());
            if (tagDto == null) {
                tagDto = new TagDto();
                tagDto.setName(tag.getName());

                tagService.create(tagDto);
            }
            tagDtos.add(tagDto);
        }
        projectDto.setTagList(tagDtos);
    }

    @Override
    public List<ProjectDto> findAll() {
        return toDtos(projectDao.findAll());
    }

    @Override
    public List<ProjectDto> getMostDownloaded() {
        return toDtos(projectDao.mostDownloaded());
    }

    @Override
    public List<ProjectDto> getMostRated() {
        return toDtos(projectDao.mostRated());
    }

    @Override
    public List<Object[]> aggregateLanguageCount() {
        return projectDao.aggregateLanguageCount();
    }

    @Override
    public List<ProjectDto> findByLanguage(String languageName) {
        return toDtos(projectDao.findByLanguage(languageName));
    }

    @Override
    public List<ProjectDto> findByTag(String tagName) {
        return toDtos(projectDao.findByTag(tagName));
    }
    
    @Override
    public List<ProjectDto> findByQuery(String query){
        return toDtos(projectDao.findByQuery(query));
    }

    @Override
    public InputStream getZipProject(Integer id) {
        try {
            String tmpdest = System.getProperty("jboss.server.temp.dir") + File.separator + id.toString() + ".zip";
            if (!new File(tmpdest).exists()) {
                boolean b = new Zip(FileSystemUtils.getProjectDataStore(id).getAbsolutePath(), tmpdest).zipIt();
                if (!b) {
                    return null;
                }
            }
            return new FileInputStream(tmpdest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(ProjectDto projectDto) {
        projectDao.update(toEntity(projectDto));
    }
}
