package cz.vut.fit.gja.services.impl;

import cz.vut.fit.gja.dao.LanguageDao;
import cz.vut.fit.gja.dto.LanguageDto;
import cz.vut.fit.gja.entities.Language;
import cz.vut.fit.gja.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 */
@Transactional
@Service
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    private LanguageDao languageDao;

    @Override
    public List<Language> findAll() {
        return languageDao.findAll();
    }

    public void setLanguageDao(LanguageDao languageDao) {
        this.languageDao = languageDao;
    }

    @Override
    public LanguageDto toDto(Language language) {
        if (language == null) {
            return null;
        }
        LanguageDto dto = new LanguageDto();
        dto.setId(language.getId());
        dto.setName(language.getName());
        dto.setHighlightMark(language.getHighlightMark());
        dto.setExtension(language.getExtension());
        return dto;
    }
}
