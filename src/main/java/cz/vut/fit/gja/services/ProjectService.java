package cz.vut.fit.gja.services;

import cz.vut.fit.gja.dao.ProjectDao;
import cz.vut.fit.gja.dto.ProjectDto;
import cz.vut.fit.gja.entities.Project;
import cz.vut.fit.gja.utils.UploadedZip;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 *
 */
public interface ProjectService {

    void create(ProjectDto projectDto);

    ProjectDto find(int id);

    ProjectDto toDto(Project project);

    Project toEntity(ProjectDto projectDto);

    ProjectDao getProjectDao();

    void saveCode(ProjectDto dto);

    void saveFile(String fileName, byte[] fileData, ProjectDto dto);

    void saveFile(String fileName, byte[] fileData, File dataStore);

    void saveZipFile(UploadedZip uploadedZip, ProjectDto dto) throws IOException;

    void saveZipFileEntries(UploadedZip uploadedZip, File dataStore) throws IOException;

    void linkWithTags(ProjectDto projectDto);

    List<ProjectDto> findAll();

    List<ProjectDto> getMostDownloaded();

    List<ProjectDto> getMostRated();

    List<Object[]> aggregateLanguageCount();

    List<ProjectDto> findByLanguage(String languageName);

    List<ProjectDto> findByTag(String tagName);

    List<ProjectDto> findByQuery(String query);

    InputStream getZipProject(Integer id);

    void update(ProjectDto projectDto);
}
