package cz.vut.fit.gja.services;

import cz.vut.fit.gja.dao.TagDao;
import cz.vut.fit.gja.dto.TagDto;
import cz.vut.fit.gja.entities.Tag;

import java.util.List;

/**
 *
 */
public interface TagService {

    void create(TagDto tagDto);

    TagDto findByName(String name);

    TagDto toDto(Tag tag);

    Tag toEntity(TagDto dto);

    List<TagDto> toDtos(List<Tag> tagList);

    List<Tag> toEntities(List<TagDto> tagList);

    TagDao getTagDao();

    List<Tag> findAll();

    Integer aggregateMaxRelevance();
}
