package cz.vut.fit.gja.services.impl;

import cz.vut.fit.gja.dto.TagDto;
import cz.vut.fit.gja.entities.Tag;
import cz.vut.fit.gja.dao.TagDao;
import cz.vut.fit.gja.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Transactional
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDao tagDao;

    @Override
    public void create(TagDto tagDto) {
        Tag t = toEntity(tagDto);
        tagDao.create(t);
        tagDto.setId(t.getId());
    }

    @Override
    public TagDto findByName(String name) {
        Tag t = tagDao.findByName(name);
        if (t != null) {
            t.setRelevance(t.getRelevance() + 1);
            return toDto(t);
        } else {
            return null;
        }
    }

    @Override
    public TagDto toDto(Tag tag) {
        TagDto dto = new TagDto();
        dto.setId(tag.getId());
        dto.setName(tag.getName());
        dto.setRelevance(tag.getRelevance());
        return dto;
    }

    @Override
    public Tag toEntity(TagDto dto) {
        Tag tag = new Tag();
        tag.setId(dto.getId() == 0 ? null : dto.getId());
        tag.setName(dto.getName());
        tag.setRelevance(dto.getRelevance());
        return tag;
    }

    @Override
    public List<TagDto> toDtos(List<Tag> tagList) {
        ArrayList<TagDto> tagDtos = new ArrayList<>();
        for (Tag tag : tagList) {
            tagDtos.add(toDto(tag));
        }
        return tagDtos;
    }

    @Override
    public List<Tag> toEntities(List<TagDto> tagList) {
        ArrayList<Tag> tags = new ArrayList<>();
        for (TagDto tag : tagList) {
            tags.add(toEntity(tag));
        }
        return tags;
    }

    @Override
    public TagDao getTagDao() {
        return tagDao;
    }

    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }

    @Override
    public List<Tag> findAll() {
        return tagDao.findAll();
    }

    @Override
    public Integer aggregateMaxRelevance() {
        return tagDao.aggregateMaxRelevance();
    }
}
