package cz.vut.fit.gja.services;

import cz.vut.fit.gja.dao.LanguageDao;
import cz.vut.fit.gja.dto.LanguageDto;
import cz.vut.fit.gja.entities.Language;

import java.util.List;

/**
 *
 */
public interface LanguageService {

    List<Language> findAll();

    LanguageDto toDto(Language language);
}
